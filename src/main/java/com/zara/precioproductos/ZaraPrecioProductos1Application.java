package com.zara.precioproductos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZaraPrecioProductos1Application {

	public static void main(String[] args) {
		SpringApplication.run(ZaraPrecioProductos1Application.class, args);
	}

}
